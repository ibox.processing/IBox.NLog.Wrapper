﻿using System;
using System.Diagnostics;
using System.Reflection;
using FluentAssertions;
using IBox.NLog.Wrapper.Contracts;
using Microsoft.Practices.Unity;
using Xunit;
using IBox.NLog.Wrapper.Unity;

namespace IBox.NLog.Wrapper.Tests
{
    /// <summary>
    /// Тестирование интерфейсов логирования
    /// </summary>
    public class AsyncLoggerTest
    {
        [Fact(DisplayName = "проверка работоспособоности логирования (интеграцонный)")]
        public async void LoggerTest()
        {
            var unity = new UnityContainer();
            unity.AddNewExtension<NLogAsyncWrapperUnity>();

            var logger = unity.Resolve<ILogerAsyncWrapper>();
            logger.Should().NotBeNull();

            var message = unity.Resolve<ILogMessage>();
            message.Should().NotBeNull();
            
            message.Exception = new NotImplementedException();
            message.Message = message.Exception.Message;
            message.MethodName = "проверка работоспособоности логирования (интеграцонный)";
            Exception ex = null;
            try
            {
                await logger.TraceAsync(message).ConfigureAwait(false);
                await logger.InfoAsync(message).ConfigureAwait(false);
                await logger.DebugAsync(message.Exception, message).ConfigureAwait(false);
                await logger.WarningAsync(message.Exception, message).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                ex = e;
            }
            ex.Should().BeNull();


        }
    }
}