﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Общие сведения об этой сборке предоставляются следующим набором
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("IBox.NLog.Wrapper.Unity")]
[assembly: AssemblyDescription("Unity container extention to logger")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("IBox")]
[assembly: AssemblyProduct("IBox.NLog.Wrapper.Unity")]
[assembly: AssemblyCopyright("Copyright ibox©  2017")]
[assembly: AssemblyTrademark("IBox")]
[assembly: AssemblyCulture("")]

// Установка значения False для параметра ComVisible делает типы в этой сборке невидимыми
// для компонентов COM. Если необходимо обратиться к типу в этой сборке через
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("a4d41313-ec0e-4c61-b8cb-2742486dc912")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номер сборки и номер редакции по умолчанию.
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.1.35")]
[assembly: AssemblyFileVersion("0.0.1.35")]
