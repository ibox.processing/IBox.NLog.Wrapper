﻿namespace IBox.NLog.Wrapper.Unity
{
    using System.Diagnostics.CodeAnalysis;
    using Contracts;
    using Microsoft.Practices.Unity;

    /// <summary>
    /// Расширение конитейнеров Unity
    /// </summary>
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class NLogAsyncWrapperUnity:UnityContainerExtension
    {
        /// <summary>
        /// Инициализация
        /// </summary>
        protected override void Initialize()
        {
            Container.RegisterType<ILogMessage, LogMessage>();
            Container.RegisterInstance<ILogerAsyncWrapper>(new NLogAsyncWrapper());
        }
    }
}