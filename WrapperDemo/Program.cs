﻿using System;
using System.Threading.Tasks;
using IBox.NLog.Wrapper.Contracts;
using Microsoft.Practices.Unity;
using IBox.NLog.Wrapper.Unity;

namespace WrapperDemo
{
    internal static class Program
    {
        private static void Main()
        {
            var containter = new UnityContainer();
            containter.AddNewExtension<NLogAsyncWrapperUnity>();
            var logger = containter.Resolve<ILogerAsyncWrapper>();
            var message = containter.Resolve<ILogMessage>();
            message.MethodName = "Тестирование работоспосопбности";
            message.Message = "Трассировка тестового метода";
            TraceLog(logger, message).ConfigureAwait(false);
            Console.ReadLine();
        }

        private static async Task TraceLog(ILogerAsyncWrapper wrapper, ILogMessage message)
        {
            await wrapper.TraceAsync(message);
            await wrapper.DebugAsync(new NotFiniteNumberException(), message);
            await wrapper.TraceAsync(new NotImplementedException(), message);
        }
    }
}
