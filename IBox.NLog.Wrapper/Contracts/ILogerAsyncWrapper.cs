﻿using System;
using System.Threading.Tasks;
namespace IBox.NLog.Wrapper.Contracts
{
    /// <summary>
    /// Loger async wrapper.
    /// </summary>
    public interface ILogerAsyncWrapper
    {
        /// <summary>
        /// Infos the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exeption">Exeption.</param>
        /// <param name="message">Message.</param>
        Task InfoAsync(Exception exeption, ILogMessage message);

        /// <summary>
        /// Traces the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exeption">Exeption.</param>
        /// <param name="message">Message.</param>
        Task TraceAsync(Exception exeption, ILogMessage message);

        /// <summary>
        /// Debugs the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exception">Exception.</param>
        /// <param name="message">Message.</param>
        Task DebugAsync(Exception exception, ILogMessage message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task InfoAsync(ILogMessage message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task TraceAsync(ILogMessage message);

        /// <summary>
        /// Warnings the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exception">Exception.</param>
        /// <param name="message">Message.</param>
        Task WarningAsync(Exception exception, ILogMessage message);
    }
}
