﻿using System;
using Newtonsoft.Json.Converters;

namespace IBox.NLog.Wrapper.Contracts
{
    /// <summary>
    /// Log message converter.
    /// </summary>
    internal sealed class LogMessageConverter : CustomCreationConverter<ILogMessage>
    {
        /// <summary>
        /// Create the specified objectType.
        /// </summary>
        /// <returns>The create.</returns>
        /// <param name="objectType">Object type.</param>
        public override ILogMessage Create(Type objectType)
        {
            return new LogMessage();
        }
    }
}
