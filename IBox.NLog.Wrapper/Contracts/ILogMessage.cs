﻿using System;
namespace IBox.NLog.Wrapper.Contracts
{
    /// <summary>
    /// Log message.
    /// </summary>
    public interface ILogMessage
    {
        /// <summary>
        /// Gets or sets the json.
        /// </summary>
        /// <value>The json.</value>
        string MethodName { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        string Message { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        Exception Exception { get; set; }
    }
}
