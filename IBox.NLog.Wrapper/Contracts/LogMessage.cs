﻿using System;

namespace IBox.NLog.Wrapper.Contracts
{
    /// <summary>
    /// Log message.
    /// </summary>
    internal sealed class LogMessage : ILogMessage
    {
        /// <summary>
        /// Gets or sets the json.
        /// </summary>
        /// <value>The json.</value>
        public string MethodName { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }
        
        /// <summary>
        /// exception 
        /// </summary>
        public Exception Exception { get; set; }

    }
}
