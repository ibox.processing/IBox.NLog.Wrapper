﻿using System.Data;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("IBox.NLog.Wrapper.Unity")]
namespace IBox.NLog.Wrapper
{
    using System;
    using System.Threading.Tasks;
    using Anotar.NLog;
    using Contracts;
    using Newtonsoft.Json;

    /// <summary>
    /// NLog async wrapper.
    /// </summary>
    internal sealed class NLogAsyncWrapper : ILogerAsyncWrapper
    {
        /// <summary>
        /// Debugs the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exception">Exception.</param>
        /// <param name="message">Message.</param>
        [LogToErrorOnException]
        [LogToDebugOnException]
        public Task DebugAsync(Exception exception, ILogMessage message)
        {
            return Task.Factory.StartNew(() =>
            {
                var json = JsonConvert.SerializeObject(message, new LogMessageConverter());
                LogTo.DebugException(()=> json, exception);
            });
        }

        /// <summary>
        /// Infos the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="message">Message.</param>
        [LogToErrorOnException]
        [LogToDebugOnException]
        public Task InfoAsync(ILogMessage message)
        {
            return Task.Factory.StartNew(() =>
            {
                var json = JsonConvert.SerializeObject(message, new LogMessageConverter());
                LogTo.Info(()=>json);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [LogToErrorOnException]
        [LogToDebugOnException]
        public Task TraceAsync(ILogMessage message)
        {
            return Task.Factory.StartNew(() =>
            {
                message.With(x => x.Exception.Do(exept =>
                {
                    LogTo.InfoException(()=> message.Message, message.Exception);
                    LogTo.DebugException(()=>message.Message, exept);
                }));
                message.With(x => x.Message.Do(mes =>
                {
                    LogTo.Info(() => mes);
                }));
            });
        }

        /// <summary>
        /// Infos the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exception">Exception.</param>
        /// <param name="message">Message.</param>
        [LogToErrorOnException]
        [LogToDebugOnException]
        public Task InfoAsync(Exception exception, ILogMessage message)
        {
            return Task.Factory.StartNew(() =>
            {
                LogTo.InfoException(JsonConvert.SerializeObject(message, new LogMessageConverter()), exception);
            });
        }

        /// <summary>
        /// Traces the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exeption">Exeption.</param>
        /// <param name="message">Message.</param>
        [LogToErrorOnException]
        [LogToDebugOnException]
        public Task TraceAsync(Exception exeption, ILogMessage message)
        {
            return Task.Factory.StartNew(() =>
            {
                var exceptionMessage =
                    JsonConvert.SerializeObject(message, Formatting.Indented, new LogMessageConverter());
                LogTo.TraceException(exceptionMessage, exeption);
            });
        }

        /// <summary>
        /// Warnings the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="exception">Exception.</param>
        /// <param name="message">Message.</param>
		[LogToErrorOnException]
        [LogToDebugOnException]
        public Task WarningAsync(Exception exception, ILogMessage message)
        {
            return Task.Factory.StartNew(() =>
            {
                LogTo.WarnException(JsonConvert.SerializeObject(message,
                                                                Formatting.Indented,
                                                                new LogMessageConverter()), exception);
            });
        }
    }
}
